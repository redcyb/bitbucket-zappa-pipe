#!/bin/bash

# Update an AWS Lambda function with new code, publishing a new version that points to the updated fuction.
# See https://docs.aws.amazon.com/cli/latest/reference/lambda/update-function-code.html
#
# Required globals:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_DEFAULT_REGION
#   FUNCTION_NAME   - update
#   ALIAS     - alias
#   FUNCTION_VERSION  - alias
#   WORK_DIR  - where your project lives
#
# Optional globals:
#   DEBUG (true or false)

SCRIPT_PATH=$(dirname "$0")
source "${SCRIPT_PATH}"/deploy-or-update-lambda.sh
source "${SCRIPT_PATH}"/alias-lambda.sh

parse_environment_variables() {
#  AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
#  AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
#  AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
  FUNCTION_NAME=${FUNCTION_NAME:?'FUNCTION_NAME variable missing.'}
  WORK_DIR=${WORK_DIR:?'FUNCTION_NAME variable missing.'}
}

echo "${FUNCTION_NAME}";
echo "${WORK_DIR}";

##enable_debug
parse_environment_variables

cd "${WORK_DIR}" || echo "No WORK_DIR: ${WORK_DIR}"
ls

if deploy; then
  echo "Created"
#  create_alias
elif update; then
  echo "Updated"
#  update_alias
fi
