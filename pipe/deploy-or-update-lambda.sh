#!/bin/bash

update() {
  info "Updating Lambda function."
  zappa update "${FUNCTION_NAME}"
  if [[ "${status}" -ne "0" ]]; then
    fail "Failed to update Lambda function code."
  fi
  info "Update command succeeded."
  success "Successfully updated function."
}

deploy() {
  info "Creating Lambda function."
  zappa deploy "${FUNCTION_NAME}"
  if [[ "${status}" -ne "0" ]]; then
    fail "Failed to create Lambda function code."
  fi
  info "Create command succeeded."
  success "Successfully created function."
}
