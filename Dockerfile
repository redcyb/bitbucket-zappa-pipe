FROM python:3.7
ENV PYTHONUNBUFFERED 1

COPY requirements.txt /
RUN pip install -r /requirements.txt

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

ARG WORK_DIR
ENV WORK_DIR $WORK_DIR

ENTRYPOINT ["/pipe.sh"]
